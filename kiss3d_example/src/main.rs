extern crate kiss3d;
extern crate nalgebra as na;

use std::cell::RefCell;
use std::rc::Rc;

use kiss3d::camera::{FirstPerson, Camera};
use kiss3d::event::{Action, WindowEvent, Key};
use kiss3d::resource::Mesh;
use na::{Vector3, UnitQuaternion, Point3};
use kiss3d::window::Window;
use kiss3d::light::Light;

fn main() {
    let mut window = Window::new("Kiss3d: cube");
    let mut cube      = window.add_cube(0.1, 0.1, 0.1);
        
    cube.set_color(1.0, 0.0, 0.0);

    window.set_light(Light::StickToCamera);


    let a = Point3::new(-1.0, -1.0, 0.0);
    let b = Point3::new(1.0, -1.0, 0.0);
    let c = Point3::new(0.0, 1.0, 0.0);

    let vertices = vec![a, b, c];
    let indices = vec![Point3::new(0u16, 1, 2)];

    let mesh = Rc::new(RefCell::new(Mesh::new(
        vertices, indices, None, None, false,
    )));
    let mut c = window.add_mesh(mesh, Vector3::new(1.0, 1.0, 1.0));

    c.set_color(1.0, 0.0, 0.0);
    c.enable_backface_culling(false);

    let eye = Point3::new(10.0f32, 10.0, 10.0);
    let at = Point3::origin();
    let mut first_person = FirstPerson::new(eye, at);
 

    for event in window.events().iter() {
        match event.value {
            WindowEvent::Key(key, Action::Release, _) => {
                if key == Key::W {
                }
            }
            _ => {}
        }
    }

    let rot = UnitQuaternion::from_axis_angle(&Vector3::y_axis(), 0.014);

    while window.render_with_camera(&mut first_person){
        cube.prepend_to_local_rotation(&rot);
        c.prepend_to_local_rotation(&rot)

    }
}
